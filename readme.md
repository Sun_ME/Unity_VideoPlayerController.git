#### Unity中视频播放控制
基于Unity提供的VideoPlayer组件实现，可控制播放进度。播放组件的自动隐藏。如图：

![在这里插入图片描述](images/1.png)
##### 部分代码如下：
1. 读取当前视频时间，设置格式部分代码
```csharp
  /// <summary>
    /// 更新视频的播放时间
    /// </summary>
    public void UpdateVideoTimeText()
    {
        videoProgress.fillAmount = ((float)videoPlayer.frame / (float)videoPlayer.frameCount);

        VideoNowTime.text = ((int)videoPlayer.time / 60).ToString("D2") + ":" +
            ((int)videoPlayer.time % 60).ToString("D2");
    }
```
2. 获取播放进度，并设置视频跳转部分代码
```csharp
 private void TrySkip(PointerEventData eventData)
    {
        Vector2 localPoint;
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(videoProgress.rectTransform, eventData.position,
                null, out localPoint))
        {
            float pct = Mathf.InverseLerp(videoProgress.rectTransform.rect.xMin,
                videoProgress.rectTransform.rect.xMax,
                localPoint.x);
            SkipToTarget(pct);
        }
    }
    private void SkipToTarget(float pct)
    {
        var frame = videoPlayer.frameCount * pct;
        videoPlayer.frame = (long)frame;
        videoPlayer.Play();
        isPause=false;
        videoControlling?.Invoke();
        playControllerImg.sprite = playSprite;
    }
```
3. 控制视频操作界面显示、关闭部分代码

```csharp
    void OnEnable()
    {
        StartCoroutine("DelayHide");
    }
    void Start()
    {
        videoController.videoPause += () =>
        {
            StopCoroutine("DelayHide");
            if (animator.GetCurrentAnimatorStateInfo(0).IsName("FadeOutAnimation"))
            {
                StartCoroutine("Show");
            }
        };

        videoController.videoControlling += () =>
        {
            StopCoroutine("DelayHide");
            if (!videoController.IsPause)
                StartCoroutine("DelayHide");
        };
    }
      public IEnumerator Show()
    {
        yield return null;
        animator.SetTrigger("FadeIn");
    }
    public IEnumerator DelayHide()
    {
        yield return new WaitForSeconds(4.0f);
        animator.SetTrigger("FadeOut");
    }
```

完整代码：

[https://gitee.com/Sun_ME/Unity_VideoPlayerController.git](https://gitee.com/Sun_ME/Unity_VideoPlayerController.git)