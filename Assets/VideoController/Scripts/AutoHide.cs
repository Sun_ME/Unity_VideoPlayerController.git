﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class AutoHide : MonoBehaviour,IEndDragHandler,IDragHandler,IPointerClickHandler
{

    void OnEnable()
    {
        StartCoroutine("Hide");
    }

    IEnumerator Hide()
    {
        yield return new WaitForSeconds(2.2f);
        this.gameObject.SetActive(false);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        StopCoroutine("Hide");
        StartCoroutine("Hide");
    }

    public void OnDrag(PointerEventData eventData)
    {
        StopCoroutine("Hide");
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        StopCoroutine("Hide");
        StartCoroutine("Hide");
    }
}
