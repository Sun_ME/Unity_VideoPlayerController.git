﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Video;
public class FadeController : MonoBehaviour, IPointerClickHandler
{
    public Animator animator;
    public VideoController videoController;
    void OnEnable()
    {
        StartCoroutine("DelayHide");
    }
    void Start()
    {
        videoController.videoPause += () =>
        {
            StopCoroutine("DelayHide");
            if (animator.GetCurrentAnimatorStateInfo(0).IsName("FadeOutAnimation"))
            {
                StartCoroutine("Show");
            }
        };

        videoController.videoControlling += () =>
        {
            StopCoroutine("DelayHide");
            if (!videoController.IsPause)
                StartCoroutine("DelayHide");
        };
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("FadeOutAnimation"))
        {
            StartCoroutine("Show");
            StartCoroutine("DelayHide");
        }
    }
    public IEnumerator Show()
    {
        yield return null;
        animator.SetTrigger("FadeIn");
    }
    public IEnumerator DelayHide()
    {
        yield return new WaitForSeconds(4.0f);
        animator.SetTrigger("FadeOut");
    }
}